def func(*args, **kwargs):
    print 'args' , args
    print 'kwargs', kwargs
    if args:
        return '{}: The Dark {}'.format(*args)
    elif kwargs:
        return '{name}: The Dark {thing}'.format(**kwargs)
    else:
        return 'Nothing'

func ('Carlos', 'Cow')
#Imprime args
func (name='Carlos', thing="Cow")
#imprime kwargs

