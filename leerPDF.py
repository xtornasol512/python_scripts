import re

rxcountpages = re.compile(r"$\s*/Type\s*/Page[/\s]", re.MULTILINE|re.DOTALL)

def countPages(filename):
    data = file(filename,"rb").read()
    return len(rxcountpages.findall(data))