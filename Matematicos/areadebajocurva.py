# -*- coding: UTF-8 -*-
"""
Calculo del area de ajo de una curva de una funcion f(x)
    a)Algoritmo Libre.
    b)Opcional: Algoritmo con el requerimiento de que se debe calcularse
    mediante la generacion de numeros aleatorios en R2(x[i],y[i])
    Dar por hecho que cuentas con una función que genera pares ordenados
    aleatorios en el intervalo [a,b] para x[i], y en el intervalo [c,d] para y[i]
"""

x1 = float(input('x1='))
x2 = float (input('x2='))
intervalos = int(input('intervalos='))
if x1 > x2:
    print('The calculated area will be negative')
# Compute delta_x for the integration interval
#
delta_x = ((x2-x1)/intervalos)
j = abs ((x2-x1)/delta_x)
i = int (j)
print('i =', i)
# initialize
n=0
A= 0.0
x = x1
# Begin Numerical Integration
while n < i:
    delta_A = x**2 + delta_x
    x = x + delta_x
    A = A + delta_A
    n = n+1

print('Area Under the Curve =', A)

