# -*- coding: UTF-8 -*-
"""
Calculo del area de ajo de una curva de una funcion f(x)
    a)Algoritmo Libre.
    b)Opcional: Algoritmo con el requerimiento de que se debe calcularse
    mediante la generacion de numeros aleatorios en R2(x[i],y[i])
    Dar por hecho que cuentas con una función que genera pares ordenados
    aleatorios en el intervalo [a,b] para x[i], y en el intervalo [c,d] para y[i]
"""

def config():
    #eq = raw_input('Ecuacion F(x) en terminos de x, sytaxsis python: ')
    leftend = int(input('Valor "a": '))
    rightend = int(input('Valor "b": '))
    n = input('Numero de Rectangulos')
    #espacio entre rectangulos
    deltaX = (rightend - leftend) / float(n)
    #print deltaX
    area = 0.0
    #xi = leftend
    #while (xi < rightend):
    for i in range(n):
        xi = leftend + deltaX*i
        altura = (xi**2) + 1
        area = area + (deltaX * altura)
        
    print "DeltaX : %f" % deltaX
    print "Posicion Xi: %d" % xi
    print "Area total: %d" % area 


if __name__ == '__main__':
    config() 