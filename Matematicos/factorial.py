# -*- coding: UTF-8 -*-
""" 
Simple código que devuelve el factorial de un numero dado
Para calcular dicho valor, hay que multiplicar el numero dado,
por su antecesor mientas sea superior a 1 
Ejemplo del factorial de 5 seria: 5 * 4 * 3 * 2 * 1 * 0 = 120
"""
n = 5
def factorial( n ):
   if n <1:   # base case
       return 1
   else:
       return n * factorial( n - 1 )  # recursive call

print "factorial Recursivo de %d es: %d" % (n, factorial(n))

def fac_iterativo(n):
    factorial= 1
    while (n !=0):
        factorial = factorial * n
        n = n -1;
    return factorial

print "Factorial iterativo %d" % fac_iterativo(n)