# -*- coding: UTF-8 -*-
"""
Calculo del area de ajo de una curva de una funcion f(x)
    a)Algoritmo Libre.
    b)Opcional: Algoritmo con el requerimiento de que se debe calcularse
    mediante la generacion de numeros aleatorios en R2(x[i],y[i])
    Dar por hecho que cuentas con una función que genera pares ordenados
    aleatorios en el intervalo [a,b] para x[i], y en el intervalo [c,d] para y[i]
"""
import sys

def config():
    global eq
    eq = raw_input('Ecuacion F(x) en terminos de x, sytaxsis python: ')
    leftend = int(input('Valor "a": '))
    rightend = int(input('Valor "b": '))
    n = int(input('Numero de Rectangulos'))
    #espacio entre rectangulos
    deltaX = (rightend - leftend) / float(n)
    #print results
    print 'Tu deltaX es :', deltaX
    print 'THe leftend sum is', LeftEndSum(n,leftend,deltaX)

def LeftEndSum(num, le, dx):
    leftSum = 0.0
    for i in range(num):
        x = le + i * dx
        h = eval(eq)
        leftSum = leftSum + h * x

    return leftSum

if __name__ == '__main__':
    config() 