# -*- coding: UTF-8 -*-
""" 
Encontrar el MCD de dos numeros
El algoritmo que se usa para sacar el MCD (GCD en ingles)
es el algoritmo de euclides. 
"""
a, b = 89, 55

def mcd(a, b):
    while b != 0:
        a, b = b, a % b
        #aux = a
        #a = b
        #b = aux % b

    return a

print "MCD iterativo: %d" % mcd(a, b)

def mcdR(a, b):
    if b == 0:
        return a
    return mcdR(b, a % b)

print "MCD Recursivo: %d" % mcdR(a, b)