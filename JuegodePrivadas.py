# -*- coding: utf-8 -*-
"""
Sólo para juego , porque la verdad en Python no se necesitan usar variables  ni funciones ni metodos privados
POr una filosofia de Guido Van Roussem tenia , y que pueden leerla aquí abajo:
http://mail.python.org/pipermail/tutor/2003-October/025932.html
"""
#Variable Privada
__hello = "hola mundo, soy una variable privada"
#imprimir variable global privada
print __hello

#Funcion  Privado
def __nobody():
    print __hello

#Llamar Funcion Privada global
__nobody()

#Ejemplo de Clase con variables y metodos privados
class Gato:
    """Clase Gato para Ejemplificar Uso de MEtodos y variables Privadas"""
    def __init__(self, nombre, color):
        self.__nombre = nombre
        self.__color = color
        self.maullar(self.__nombre, self.__color)
        self.__dicc = { 'nombre' : self.__nombre, 'color' : self.__color}
    

    #metodo publico de la Clase Gato
    def maullar(self, nombre, color):
        print "hola gatito ^.^ " + nombre
        print "Tu color es %s" % (color)

    def __jujuPrivado(self):
        print '"En realidad no necesitas usarme privadamente", Creador de Python'
    

#Ejemplo de Herencia
class Gary(Gato):
    pass



#LA clase heredada para usar el metodo privado debe llamar a la clase original que lo tenga
g = Gary('gary', 'negra')
g._Gato__jujuPrivado()

print g._Gato__dicc.items()


#Aqui si se peude utilizar el emetodo privado de esta manera
g2 = Gato('misifus', 'blacky')
g2._Gato__jujuPrivado()
print g2._Gato__dicc.items()

#Fuente de Informacion
#http://www.gulic.org/almacen/diveintopython-5.4-es/object_oriented_framework/private_functions.html
