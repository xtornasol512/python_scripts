#!/usr/bin/env python
#-*- coding: utf-8 -*-
#Aplicación web
from gi.repository import Gtk, WebKit
import os, sys
class Browser:
    def __init__(self):
        self.window = Gtk.Window()
        self.window.set_default_size(800, 600)
        view = WebKit.WebView()
        #sustituye http://localhost/ por lo que necesites mostrar
        view.open("http://google.com")
        self.window.add(view)
        self.window.show_all()
        self.window.connect('destroy', lambda w: Gtk.main_quit())
def main():
    app = Browser()
    Gtk.main()
if __name__ == "__main__":
    main()